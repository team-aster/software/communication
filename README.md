# Communication database

This database defines both telemetry and telecommands between the main board and the ground station.
It is spread out over multiple files and folders.
A detailed documentation about each file and directory can be found
[here](https://ecom.readthedocs.io/en/latest/database/README.html).

## Library

The [`ECom`](ECom) folder contains a Python library to parse and serialize communication packages.
It also contains a code generator which can generate a C parser and serializer.
